// 用户名模块的所有的数据请求
import request from '@/utils/request'
// import store from '@/store'

// 登录请求
export const login = (data) => {
  return request({
    url: '/v1_0/authorizations',
    method: 'POST',
    data
  })
}
export const getSms = data => {
  return request({
    url: `/v1_0/sms/codes/${data}`,
    method: 'GET'
  })
}
// 获取用户资料
export const getUserInfo = () => {
  return request({
    url: '/v1_0/user',
    method: 'GET'
    // headers: {
    //   Authorization: 'Bearer ' + store.getters.token
    // }

  })
}
/**
 * 添加关注
 */
export const addFollow = userId => {
  return request({
    method: 'POST',
    url: '/v1_0/user/followings',
    data: {
      target: userId
    }
  })
}

/**
 * 取消关注
 */
export const deleteFollow = userId => {
  return request({
    method: 'DELETE',
    url: `/v1_0/user/followings/${userId}`
  })
}
