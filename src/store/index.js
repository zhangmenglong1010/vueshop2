import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storage'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 用户token信息
    user: getItem('HEIMA_USER') || {}
  },
  getters: {
    token: state => state.user.token
  },
  mutations: {
    // 设置用户信息
    setUser (state, data) {
      // 设置给state
      state.user = data
      // 设置本地存储
      setItem('HEIMA_USER', data)
    }
  },
  actions: {
  },
  modules: {
  }
})
