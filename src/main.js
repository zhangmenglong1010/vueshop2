import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './styles/index.less'

import Vant from 'vant'
import 'vant/lib/index.css'
import 'amfe-flexible'
// 引入路由模块
import VueRouter from 'vue-router'
// 导入自定义过滤器函数
import * as filters from '@/filter'

// 重写push方法
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return routerPush.call(this, location).catch(error => error)
}

// 批量注册过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
// 挂载路由模块
Vue.use(VueRouter)

Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
