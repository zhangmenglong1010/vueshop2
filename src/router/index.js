import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  // 登录路由
  {
    path: '/login',
    component: () => import('@/views/login'),
    meta: { title: '登录' }
  },
  // 主界面路由
  {
    path: '/',
    component: () => import('@/views/layout'),
    redirect: '/home',
    children: [
      { path: '/home', component: () => import('@/views/home'), meta: { title: '首页' } },
      { path: '/qa', component: () => import('@/views/qa'), meta: { title: '问答' } },
      { path: '/video', component: () => import('@/views/video'), meta: { title: '视屏' } },
      { path: '/user', component: () => import('@/views/user'), meta: { title: '我的' } }
    ]
  },
  {
    path: '/user/shoucang',
    component: () => import('@/views/user/shoucang.vue'),
    meta: { title: '我的收藏', needLogin: true }
  },
  {
    path: '/user/lishi',
    component: () => import('@/views/user/lishi.vue'),
    meta: { title: '我的历史', needLogin: true }
  },
  {
    path: '/search',
    component: () => import('@/views/search'),
    meta: { title: '搜索' }
  },
  {
    meta: { title: '文章详情' },
    path: '/article/:id',
    name: 'article',
    component: () => import('@/views/article'),
    // 将路由动态参数映射到组件的 props 中，更推荐这种做法
    props: true
  }
]

const router = new VueRouter({
  routes
})
// 导航守卫
router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  // 登录的守卫
  // 判断去的页面是否需要登录
  if (to.meta.needLogin) {
    if (store.getters.token) {
      next()
    } else {
      next('/login?url=' + to.path)
    }
  } else {
    next()
  }
})

export default router
