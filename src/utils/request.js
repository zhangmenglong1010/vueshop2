import axios from 'axios' // 导入axios
import store from '../store'
import JSONBig from 'json-bigint'

// 创建axios实例对象
const $axios = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  timeout: 5000,
  transformResponse: [function (data) {
    try {
      return JSONBig.parse(data)
    } catch (error) {
      return data
    }
  }]
})
// 请求拦截器
$axios.interceptors.request.use(config => {
  if (store.getters.token) {
    config.headers.Authorization = 'Bearer ' + store.getters.token
  }
  return config
}, error => {
  return Promise.reject(error)
})

// 暴露这个axios实例
export default $axios
